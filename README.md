# Automated Lung Segmentation

This project hosts a set of tools and applications performing automated lung segmentations and analysis, enhancing the clinical workflows.





**Lung Lobes Segmentation Tool**:


This tool facilitates automated segmentations of lung lobes. Lung lobe segmentations are often
a primary step in radiological analysis of chest x-ray images. Automated segmentations in this
area can assist the clinicians analyzing the radiographs immensely. They do so in a couple of ways.
Firstly, they can free up initial time spent in preliminary processes including the manual
marking of these lobes. Secondly, it helps them to focus more on the final analysis and diagnosis
of the state of lungs in the radiographs. Thus, it significantly can speed up the clinical
workflow, thereby having a substantial positive impact on the quality of diagnosis as well.


Technical specs:
1. Download and install Python >= 3 from [here](https://www.python.org/downloads/).

2. Install pip(package installer for python) from [here](https://pip.pypa.io/en/stable/installing/).

3. Install Miniconda appropriate for your system from [here](https://docs.conda.io/en/latest/miniconda.html).

4. Once Miniconda is successfully installed, you can easily create an environment from the attached
environment.yml file. This can be  by entering your environment name in the following command into your Terminal(MacOS/Linux)/PowerShell(Windows):

$cd seg_app

$conda env create --name your_environment_name --file=environment.yml


5. You can verify and activate your environment by following command:

$conda activate your_environment_name


6. Once the environment is activated, you can run the segmentation application script as:

$cd seg_app 

$python lung_seg_desktop_app.py


In case you'd like to manage multiple environments, you can refer [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)

You can select and segment lung lobes from the chest x-ray radiographs (in dicom format) in an automated way using the app.